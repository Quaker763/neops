#ifndef COP0_HPP_INCLUDED
#define COP0_HPP_INCLUDED

#define COP0_MAX_REGS 16
#define COP0_MAX_TLB_ENTRIES 64

namespace cpu
{
    /**
     *  Our R3000A's first co-processor. Performs various operations in relation to
     *  the system's memory management, system interrupt management (exceptions) and breakpoints.
     */
    class cop0
    {
    public:
    private:
        std::uint32_t creg[COP0_MAX_REGS]; /**< 16 32-bit control registers */
        std::uint32_t tlb[COP0_MAX_TLB_ENTRIES]; /**< Our TLB, which contains 64 4kb page entries, which is 256MiB of virtual memory */
    };
}

#endif // COP0_HPP_INCLUDED
